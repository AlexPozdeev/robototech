﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using Manipulators;
using Transformation;
using Robot;

namespace Drawing
{
    public class ManipulatorPainter
    {
        private HingePattern _graphicalHinge;
        private PistonPattern _graphicalPiston;
        private PlankPattern _graphicalPlank;
        private GroundPattern _ground;
        BufferedGraphicsContext _buffer = BufferedGraphicsManager.Current;
        private delegate PointAnglePair PartPainter(Graphics canvas, BoundedPart hinge, int id, PointAnglePair pair);
        private Dictionary<PartType, PartPainter> _painters = new Dictionary<PartType, PartPainter>();

        public ManipulatorPainter()
        {
            _graphicalHinge = new HingePattern();
            _graphicalPiston = new PistonPattern();
            _graphicalPlank = new PlankPattern();
            _ground = new GroundPattern();
            InitializePainters();
        }

        public ManipulatorPainter(Pen contour, SolidBrush filling)
        {
            _graphicalHinge = new HingePattern(contour, filling);
            _graphicalPiston = new PistonPattern(contour, filling);
            _graphicalPlank = new PlankPattern(contour, filling);
            _ground = new GroundPattern(contour, filling);
            InitializePainters();
        }

        public ManipulatorPainter(Color background)
        {
            _graphicalHinge = new HingePattern(background);
            _graphicalPiston = new PistonPattern(background);
            _graphicalPlank = new PlankPattern(background);
            _ground = new GroundPattern(background);
            InitializePainters();
        }

        public ManipulatorPainter(Pen contour, SolidBrush filling, Color background)
        {
            _graphicalHinge = new HingePattern(contour, filling, background);
            _graphicalPiston = new PistonPattern(contour, filling, background);
            _graphicalPlank = new PlankPattern(contour, filling, background);
            _ground = new GroundPattern(contour, filling, background);
            InitializePainters();
        }

        private void InitializePainters()
        {
            _painters.Add(PartType.Hinge, _graphicalHinge.Draw);
            _painters.Add(PartType.Piston, _graphicalPiston.Draw);
            _painters.Add(PartType.Plank, _graphicalPlank.Draw);
        }

        public void Draw(Graphics canvas, Robot.Robot robot, PointF rootPosition, double directionAngle)
        {
            var robotParts = robot.Parts;
            Dictionary<int, PointAnglePair> hinges = new Dictionary<int, PointAnglePair>();
            PointAnglePair pair = new PointAnglePair(rootPosition, directionAngle);
            PartType typeId;
            BufferedGraphics bufferedGraphics = _buffer.Allocate(canvas, new Rectangle(0, 0, 1000, 1000));
            Graphics newCanvas = bufferedGraphics.Graphics;
            newCanvas.FillRectangle(_ground.FillStyle, 0, 0, 1000, 1000);
            for (int i = 0; i < robotParts.Count(); ++i)
            {
                typeId = robot[i].PartType;
                if (typeId == PartType.Hinge)
                {
                    hinges.Add(i, pair);
                    pair.angle += robot[i].CurrentValue;
                }
                else
                    pair = _painters[typeId](newCanvas, robot[i], i, pair);
            }
            foreach (var hingeInfo in hinges)
                _graphicalHinge.Draw(newCanvas, robot[hingeInfo.Key], hingeInfo.Key, hingeInfo.Value);
            _ground.Draw(newCanvas, rootPosition, directionAngle);
            bufferedGraphics.Render(canvas);
            bufferedGraphics.Dispose();
        }
    }

    struct PointAnglePair
    {
        public PointF point;
        public double angle;

        public PointAnglePair(PointF position, double direction)
        {
            point = position;
            angle = direction;
        }
    }

    class Pattern
    {
        public Pattern()
        {
            ContourStyle = new Pen(Color.Black, 1);
            Background = SystemColors.Control;
            FillStyle = new SolidBrush(Background);
        }

        public Pattern(Pen contour, SolidBrush filling)
        {
            ContourStyle = contour;
            FillStyle = filling;
            Background = SystemColors.Control;
        }

        public Pattern(Color background)
        {
            ContourStyle = new Pen(Color.Black, 1);
            Background = background;
            FillStyle = new SolidBrush(Background);
        }

        public Pattern(Pen contour, SolidBrush filling, Color background)
        {
            ContourStyle = contour;
            FillStyle = filling;
            Background = background;
        }

        public Pen ContourStyle { get; protected set; }
        public SolidBrush FillStyle { get; protected set; }
        public Color Background { get; protected set; }

        protected void DrawId(Graphics canvas, PointF center, int id)
        {
            int fontSize = 12;
            using (Font font = new Font("Times New Roman", fontSize, FontStyle.Bold))
            {
                float x = center.X - fontSize / 2;
                float y = center.Y - fontSize * 3 / 4;
                canvas.DrawString(id.ToString(), font, new SolidBrush(Color.Red), new PointF(x, y));
            }
        }
    }

    class HingePattern : Pattern
    {
        public HingePattern() : base() { }

        public HingePattern(Pen contour, SolidBrush filling) : base(contour, filling) { }

        public HingePattern(Color background) : base(background) { }

        public HingePattern(Pen contour, SolidBrush filling, Color background) : base(contour, filling, background) { }

        private float _hingeWidth = 30;
        private float _hingeInnerCircleWidth = 20;
        private PointF _backConnector = new PointF(-20, 0);
        private PointF _forwardConnector = new PointF(20, 0);

        public PointAnglePair Draw(Graphics canvas, BoundedPart hinge, int id, PointAnglePair pair)
        {
            PointF center = pair.point;
            double directionAngle = pair.angle;
            DrawBackConnector(canvas, hinge, center, directionAngle);
            DrawArc(canvas, hinge, center, directionAngle);
            DrawForwardConnector(canvas, hinge, center, directionAngle);
            DrawInnerCircle(canvas, center);
            DrawId(canvas, center, id);
            return new PointAnglePair(center, directionAngle + hinge.CurrentValue);
        }

        private void DrawBackConnector(Graphics canvas, BoundedPart hinge, PointF center, double directionAngle)
        {
            PointF actualBackConnector = TransformUtils.RotatePoint(_backConnector, directionAngle);
            TransformUtils.TranslatePoint(ref actualBackConnector, center);
            canvas.DrawLine(ContourStyle, actualBackConnector, center);
        }

        private void DrawForwardConnector(Graphics canvas, BoundedPart hinge, PointF center, double directionAngle)
        {
            double forwardConnectorAngle = directionAngle + hinge.CurrentValue;
            PointF actualForwardConnector = TransformUtils.RotatePoint(_forwardConnector, forwardConnectorAngle);
            TransformUtils.TranslatePoint(ref actualForwardConnector, center);
            canvas.DrawLine(ContourStyle, actualForwardConnector, center);
        }

        private void DrawArc(Graphics canvas, BoundedPart hinge, PointF center, double directionAngle)
        {
            PointF outerCorner = center;
            TransformUtils.TranslatePoint(ref outerCorner, new PointF(-_hingeWidth / 2, -_hingeWidth / 2));
            RectangleF outerCircleRegion = new RectangleF(outerCorner, new SizeF(_hingeWidth, _hingeWidth));
            float startArc = (float)(-hinge.MinValue - directionAngle);
            float endArc = (float)(360 - hinge.MaxValue + hinge.MinValue);
            canvas.FillEllipse(FillStyle, outerCircleRegion);
            canvas.DrawArc(ContourStyle, outerCircleRegion, startArc, endArc);
        }

        private void DrawInnerCircle(Graphics canvas, PointF center)
        {
            float innerCircleX = center.X - _hingeInnerCircleWidth / 2;
            float innerCircleY = center.Y - _hingeInnerCircleWidth / 2;
            RectangleF innerCircleRegion = new RectangleF(innerCircleX, innerCircleY, _hingeInnerCircleWidth, _hingeInnerCircleWidth);
            canvas.FillEllipse(FillStyle, innerCircleRegion);
            canvas.DrawEllipse(ContourStyle, innerCircleRegion);
        }
    }

    class PistonPattern : Pattern
    {
        public PistonPattern() : base() { }

        public PistonPattern(Pen contour, SolidBrush filling) : base(contour, filling) { }

        public PistonPattern(Color background) : base(background) { }

        public PistonPattern(Pen contour, SolidBrush filling, Color background) : base(contour, filling, background) { }

        private PointF[] _centralRectangle =
                { new PointF(-15, -8), new PointF(15, -8), new PointF(15, 8), new PointF(-15, 8) };

        public PointAnglePair Draw(Graphics canvas, BoundedPart piston, int id, PointAnglePair pair)
        {
            PointF begin = pair.point;
            double angle = pair.angle;
            PointF end = DrawBaseLine(canvas, piston, begin, angle);
            DrawCentralRectangle(canvas, begin, end, angle);
            PointF center = TransformUtils.GetMiddlePoint(begin, end);
            DrawId(canvas, center, id);
            return new PointAnglePair(end, angle);
        }

        private PointF DrawBaseLine(Graphics canvas, BoundedPart piston, PointF begin, double directionAngle)
        {
            PointF end = TransformUtils.GetEndPoint(begin, directionAngle, piston.CurrentValue);
            canvas.DrawLine(ContourStyle, begin, end);
            return end;
        }

        private void DrawCentralRectangle(Graphics canvas, PointF begin, PointF end, double directionAngle)
        {
            PointF[] actualCentralRectangle = TransformUtils.RotatePoints(directionAngle, _centralRectangle);
            PointF center = TransformUtils.GetMiddlePoint(begin, end);
            TransformUtils.TranslatePoints(center, ref actualCentralRectangle);
            canvas.FillPolygon(FillStyle, actualCentralRectangle);
            for (int i = 0; i < actualCentralRectangle.Count() - 1; ++i)
                for (int j = i + 1; j < actualCentralRectangle.Count(); ++j)
                    canvas.DrawLine(ContourStyle, actualCentralRectangle[i], actualCentralRectangle[j]);
        }
    }

    class PlankPattern : Pattern
    {
        public PlankPattern() : base() { }

        public PlankPattern(Pen contour, SolidBrush filling) : base(contour, filling) { }

        public PlankPattern(Color background) : base(background) { }

        public PlankPattern(Pen contour, SolidBrush filling, Color background) : base(contour, filling, background) { }

        public PointAnglePair Draw(Graphics canvas, BoundedPart plank, int id, PointAnglePair pair)
        {
            PointF begin = pair.point;
            PointF end = TransformUtils.GetEndPoint(begin, pair.angle, plank.CurrentValue);
            canvas.DrawLine(ContourStyle, begin, end);
            PointF center = TransformUtils.GetMiddlePoint(begin, end);
            DrawId(canvas, center, id);
            return new PointAnglePair(end, pair.angle);
        }
    }

    class GroundPattern : Pattern
    {
        public GroundPattern() : base()
        {
            Initialize();
        }

        public GroundPattern(Pen contour, SolidBrush filling) : base(contour, filling)
        {
            Initialize();
        }

        public GroundPattern(Color background) : base(background)
        {
            Initialize();
        }

        public GroundPattern(Pen contour, SolidBrush filling, Color background) : base(contour, filling, background)
        {
            Initialize();
        }

        private PointF[] _surface = { new PointF(0, 30), new PointF(0, -30) };
        private PointF[] _hatching = new PointF[12];
        private PointF[] _groundArea = { new PointF(-25, -30), new PointF(0, -30),
            new PointF(0, 30), new PointF(-25, 30) };

        private void Initialize()
        {
            int topPosition = -30;
            int lineLength = 10;
            PointF line = new PointF(-lineLength, 0);
            for (int i = 0; i < _hatching.Length; i += 2)
            {
                _hatching[i] = new PointF(0, topPosition);
                PointF actualLine = TransformUtils.RotatePoint(line, 45);
                _hatching[i + 1] = TransformUtils.TranslatePoint(actualLine, new PointF(0, topPosition));
                topPosition += lineLength;
            }
        }

        public void Draw(Graphics canvas, PointF center, double angle)
        {
            ClearArea(canvas, center, angle);
            DrawSurface(canvas, center, angle);
            DrawHatching(canvas, center, angle);
        }

        private void ClearArea(Graphics canvas, PointF center, double angle)
        {
            PointF[] actualArea = TransformUtils.RotatePoints(angle, _groundArea);
            TransformUtils.TranslatePoints(center, ref actualArea);
            canvas.FillPolygon(FillStyle, actualArea);
        }

        private void DrawSurface(Graphics canvas, PointF center, double angle)
        {
            PointF[] actualSurface = TransformUtils.RotatePoints(angle, _surface);
            TransformUtils.TranslatePoints(center, ref actualSurface);
            canvas.DrawLine(ContourStyle, actualSurface[0], actualSurface[1]);
        }

        private void DrawHatching(Graphics canvas, PointF center, double angle)
        {
            for (int i = 0; i < _hatching.Length; i += 2)
            {
                PointF lineBegin = _hatching[i];
                PointF lineEnd = _hatching[i + 1];
                TransformUtils.RotatePoint(ref lineBegin, angle);
                TransformUtils.TranslatePoint(ref lineBegin, center);
                TransformUtils.RotatePoint(ref lineEnd, angle);
                TransformUtils.TranslatePoint(ref lineEnd, center);
                canvas.DrawLine(ContourStyle, lineBegin, lineEnd);
            }
        }
    }
}
