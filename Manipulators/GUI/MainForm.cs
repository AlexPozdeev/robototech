﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Drawing;
using Manipulators;


namespace GUI
{
    public partial class Form1 : Form
    {
        double StartAngle;
        Graphics Graphic;
        PointF StartPoint;
        ManipulatorPainter Painter;
        Robot.Robot Megatron;
        

        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void соединениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddPiston PistonAddingForm = new AddPiston(ref Megatron);
            PistonAddingForm.Text = "Добавить поршень";
            PistonAddingForm.ShowDialog();

            ListElements.Items.Clear();
            valueSlider.Enabled = false;
            applyCurrentValue.Enabled = false;
            int i = 0;
            foreach (BoundedPart Part in Megatron.Parts)
            {
                ListElements.Items.Add(i++ + ":" + Part.PartType.ToString());
            }
            DrawMegatron();
        }

        private void узелToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddHinge HingeAddingForm = new AddHinge(ref Megatron);
            HingeAddingForm.Text = "Добавить узел";
            HingeAddingForm.ShowDialog();
            ListElements.Items.Clear();
            valueSlider.Enabled = false;
            applyCurrentValue.Enabled = false;
            int i = 0;
            foreach (BoundedPart Part in Megatron.Parts)
            {
                ListElements.Items.Add(i++ + ":" + Part.PartType.ToString());
            }
            DrawMegatron();
        }

        private void DecreaseButton_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            StartAngle = 90;
            float x = PictureBox.Width / 2;
            float y = PictureBox.Height / 2;
            Painter = new ManipulatorPainter();
            Megatron = new Robot.Robot();
            StartPoint = new PointF(x, y);
            Graphic = PictureBox.CreateGraphics();
            ListElements.Items.Add("0" + ":" + Megatron.Parts.Last.Value.PartType.ToString());
            DrawMegatron();
            PictureBox.Refresh();
        }

        private void очиститьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddPlank PlankAddingForm = new AddPlank(ref Megatron);
            PlankAddingForm.Text = "Добавить планку";
            PlankAddingForm.ShowDialog();

            ListElements.Items.Clear();
            valueSlider.Enabled = false;
            applyCurrentValue.Enabled = false;
            int i = 0;
            foreach(BoundedPart Part in Megatron.Parts)
            {
                ListElements.Items.Add(i++ + ":" + Part.PartType.ToString());
            }
            DrawMegatron();
        }

        private void DrawMegatron()
        {
            Painter.Draw(Graphic, Megatron, StartPoint, StartAngle);
        }

        private void отрисоватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DrawMegatron();
        }

        private void ListElements_SelectedIndexChanged(object sender, EventArgs e)
        {
            int partNo = ListElements.SelectedIndex;
            if (Megatron[partNo].PartType == PartType.Plank)
            {
                valueSlider.Enabled = false;
                applyCurrentValue.Enabled = false;
                return;
            }
            double minValue = Megatron[partNo].MinValue;
            double maxValue = Megatron[partNo].MaxValue;
            double currentValue = Megatron[partNo].CurrentValue;
            valueSlider.Minimum = (int)minValue;
            valueSlider.Maximum = (int)maxValue;
            valueSlider.Value = (int)currentValue;
            valueSlider.Enabled = true;
            applyCurrentValue.Enabled = true;
        }

        private void applyCurrentValue_Click(object sender, EventArgs e)
        {
            int frameNumber = 100;
            int partNo = ListElements.SelectedIndex;
            double error = 5;
            double endValue = valueSlider.Value;
            double currentValue = Megatron[partNo].CurrentValue;
            double step = (endValue - currentValue) / frameNumber;
            while (Math.Abs(Megatron[partNo].CurrentValue - endValue) > error)
            {
                Megatron[partNo].AddValue(step);
                DrawMegatron();
            }
        }

        private void valueSlider_ValueChanged(object sender, EventArgs e)
        {
            sliderNumberLabel.Text = valueSlider.Value.ToString();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        static bool firstPaint = true;
        private void PictureBox_Paint(object sender, PaintEventArgs e)
        {
            if(firstPaint)
            {
                Graphics canvas = e.Graphics;
                Painter.Draw(canvas, Megatron, StartPoint, StartAngle);
                firstPaint = false;
            }
        }
    }
}
