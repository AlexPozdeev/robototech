﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Robot;
using Manipulators;

namespace GUI
{
    public partial class AddHinge : Form
    {
        Robot.Robot Megatron;

        public AddHinge(ref Robot.Robot Robot)
        {
            InitializeComponent();
            Megatron = Robot;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double Value = double.Parse(textBox1.Text);
            double MaxValue = double.Parse(textBox2.Text);
            double MinValue = double.Parse(textBox3.Text);

            Hinge Hinge = new Hinge(MinValue, MaxValue, Value);
            Hinge.Position = Megatron.Parts.Last.Value.Position;
            Megatron.AddPartToEnd(Hinge);
            this.Close();
        }
    }
}
