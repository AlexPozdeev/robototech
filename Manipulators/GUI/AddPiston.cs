﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Robot;
using Manipulators;

namespace GUI
{
    public partial class AddPiston : Form
    {
        Robot.Robot Megatron;
        public AddPiston(ref Robot.Robot Robot)
        {
            InitializeComponent();
            Megatron = Robot;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double Value = double.Parse(textBox1.Text);
            double MaxValue = double.Parse(textBox2.Text);
            double MinValue = double.Parse(textBox3.Text);

            Piston Piston = new Manipulators.Piston(MinValue,MaxValue,Value);
            Piston.Position = Megatron.Parts.Last.Value.Position;
            Megatron.AddPartToEnd(Piston);
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
