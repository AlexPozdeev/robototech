﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Robot;
using Manipulators;

namespace GUI
{
    public partial class AddPlank : Form
    {
        Robot.Robot Megatron;

        public AddPlank(ref Robot.Robot Robot)
        {
            InitializeComponent();
            Megatron = Robot;
        }

        private void AddElement_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Plank Plank = new Plank(Int32.Parse(textBox1.Text));
            Plank.Position = Megatron.Parts.Last.Value.Position;
            Megatron.AddPartToEnd(Plank);
            this.Close();
        }
    }
}
