﻿namespace GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.узелToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.соединениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.рисованиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отрисоватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ListElements = new System.Windows.Forms.ListBox();
            this.IncreaseButton = new System.Windows.Forms.Button();
            this.DecreaseButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.sliderNumberLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.applyCurrentValue = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.valueSlider = new System.Windows.Forms.TrackBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PictureBox = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valueSlider)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьToolStripMenuItem,
            this.рисованиеToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(727, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.добавитьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.узелToolStripMenuItem,
            this.соединениеToolStripMenuItem,
            this.очиститьToolStripMenuItem});
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.добавитьToolStripMenuItem.Text = "Добавить";
            this.добавитьToolStripMenuItem.Click += new System.EventHandler(this.добавитьToolStripMenuItem_Click);
            // 
            // узелToolStripMenuItem
            // 
            this.узелToolStripMenuItem.Name = "узелToolStripMenuItem";
            this.узелToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.узелToolStripMenuItem.Text = "Узел";
            this.узелToolStripMenuItem.Click += new System.EventHandler(this.узелToolStripMenuItem_Click);
            // 
            // соединениеToolStripMenuItem
            // 
            this.соединениеToolStripMenuItem.Name = "соединениеToolStripMenuItem";
            this.соединениеToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.соединениеToolStripMenuItem.Text = "Поршень";
            this.соединениеToolStripMenuItem.Click += new System.EventHandler(this.соединениеToolStripMenuItem_Click);
            // 
            // очиститьToolStripMenuItem
            // 
            this.очиститьToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.очиститьToolStripMenuItem.Name = "очиститьToolStripMenuItem";
            this.очиститьToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.очиститьToolStripMenuItem.Text = "Планка";
            this.очиститьToolStripMenuItem.Click += new System.EventHandler(this.очиститьToolStripMenuItem_Click);
            // 
            // рисованиеToolStripMenuItem
            // 
            this.рисованиеToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.рисованиеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отрисоватьToolStripMenuItem});
            this.рисованиеToolStripMenuItem.Name = "рисованиеToolStripMenuItem";
            this.рисованиеToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.рисованиеToolStripMenuItem.Text = "Рисование";
            // 
            // отрисоватьToolStripMenuItem
            // 
            this.отрисоватьToolStripMenuItem.Name = "отрисоватьToolStripMenuItem";
            this.отрисоватьToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.отрисоватьToolStripMenuItem.Text = "Отрисовать";
            this.отрисоватьToolStripMenuItem.Click += new System.EventHandler(this.отрисоватьToolStripMenuItem_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel3.Controls.Add(this.ListElements);
            this.panel3.Location = new System.Drawing.Point(505, 31);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(187, 222);
            this.panel3.TabIndex = 3;
            // 
            // ListElements
            // 
            this.ListElements.FormattingEnabled = true;
            this.ListElements.Location = new System.Drawing.Point(3, 3);
            this.ListElements.Name = "ListElements";
            this.ListElements.Size = new System.Drawing.Size(181, 212);
            this.ListElements.TabIndex = 0;
            this.ListElements.SelectedIndexChanged += new System.EventHandler(this.ListElements_SelectedIndexChanged);
            // 
            // IncreaseButton
            // 
            this.IncreaseButton.Location = new System.Drawing.Point(504, 259);
            this.IncreaseButton.Name = "IncreaseButton";
            this.IncreaseButton.Size = new System.Drawing.Size(75, 23);
            this.IncreaseButton.TabIndex = 1;
            this.IncreaseButton.Text = "+";
            this.IncreaseButton.UseVisualStyleBackColor = true;
            // 
            // DecreaseButton
            // 
            this.DecreaseButton.Location = new System.Drawing.Point(617, 259);
            this.DecreaseButton.Name = "DecreaseButton";
            this.DecreaseButton.Size = new System.Drawing.Size(75, 23);
            this.DecreaseButton.TabIndex = 2;
            this.DecreaseButton.Text = "-";
            this.DecreaseButton.UseVisualStyleBackColor = true;
            this.DecreaseButton.Click += new System.EventHandler(this.DecreaseButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(546, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Список элементов";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel2.Controls.Add(this.sliderNumberLabel);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.applyCurrentValue);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.valueSlider);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DecreaseButton);
            this.panel2.Controls.Add(this.IncreaseButton);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(12, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(703, 463);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // sliderNumberLabel
            // 
            this.sliderNumberLabel.AutoSize = true;
            this.sliderNumberLabel.Location = new System.Drawing.Point(590, 364);
            this.sliderNumberLabel.Name = "sliderNumberLabel";
            this.sliderNumberLabel.Size = new System.Drawing.Size(13, 13);
            this.sliderNumberLabel.TabIndex = 10;
            this.sliderNumberLabel.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(549, 313);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Текущее значение";
            // 
            // applyCurrentValue
            // 
            this.applyCurrentValue.Location = new System.Drawing.Point(561, 383);
            this.applyCurrentValue.Name = "applyCurrentValue";
            this.applyCurrentValue.Size = new System.Drawing.Size(75, 23);
            this.applyCurrentValue.TabIndex = 8;
            this.applyCurrentValue.Text = "Задать";
            this.applyCurrentValue.UseVisualStyleBackColor = true;
            this.applyCurrentValue.Click += new System.EventHandler(this.applyCurrentValue_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(655, 364);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Макс.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(505, 364);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Мин.";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // valueSlider
            // 
            this.valueSlider.Location = new System.Drawing.Point(508, 332);
            this.valueSlider.Name = "valueSlider";
            this.valueSlider.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.valueSlider.Size = new System.Drawing.Size(184, 45);
            this.valueSlider.TabIndex = 5;
            this.valueSlider.TickStyle = System.Windows.Forms.TickStyle.None;
            this.valueSlider.ValueChanged += new System.EventHandler(this.valueSlider_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.PictureBox);
            this.panel1.Location = new System.Drawing.Point(18, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(481, 386);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // PictureBox
            // 
            this.PictureBox.BackColor = System.Drawing.SystemColors.Control;
            this.PictureBox.Location = new System.Drawing.Point(3, 3);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(475, 379);
            this.PictureBox.TabIndex = 0;
            this.PictureBox.TabStop = false;
            this.PictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBox_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 453);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "MegaTron";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valueSlider)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem узелToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem соединениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem рисованиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отрисоватьToolStripMenuItem;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListBox ListElements;
        private System.Windows.Forms.Button IncreaseButton;
        private System.Windows.Forms.Button DecreaseButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox PictureBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button applyCurrentValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar valueSlider;
        private System.Windows.Forms.Label sliderNumberLabel;
    }
}

