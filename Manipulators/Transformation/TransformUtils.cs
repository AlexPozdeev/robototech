﻿using System;
using System.Collections.Generic;
using System.Drawing;
using MatrixProject;

namespace Transformation
{
    public static class TransformUtils
    {
        /// <summary>
        /// Поворачивает точку
        /// </summary>
        /// <param name="pointToRotate">Заданная точка</param>
        /// <param name="angle">Угол в градусах</param>
        /// <returns>Точка с новыми координатами</returns>
        public static PointF RotatePoint(PointF pointToRotate, double angle)
        {
            var angleInRadians = DegreesToRadians(angle);

            var coordsVectorElementsList = new List<double>()
            {
                pointToRotate.X,
                pointToRotate.Y
            };

            var rotationMatrixElementsList = new List<double>()
            {
                Math.Cos(angleInRadians),
                Math.Sin(angleInRadians),
                -Math.Sin(angleInRadians),
                Math.Cos(angleInRadians)
            };

            var coordsVector = new CMatrix2D(1, 2, coordsVectorElementsList);
            var rotationMatrix = new CMatrix2D(2, 2, rotationMatrixElementsList);
            var result = rotationMatrix.Mult(coordsVector);

            return new PointF((float)result.GetItem(0, 0), (float)result.GetItem(1, 0));
        }

        /// <summary>
        /// Попорачивает точку
        /// </summary>
        /// <param name="pointToRotate">Заданная точка</param>
        /// <param name="angle">Угол в градусах</param>
        public static void RotatePoint(ref PointF pointToRotate, double angle)
        {
            var newPoint = RotatePoint(pointToRotate, angle);
            pointToRotate.X = newPoint.X;
            pointToRotate.Y = newPoint.Y;
        }

        /// <summary>
        /// Перемещает точку
        /// </summary>
        /// <param name="pointToTranslate">Заданная точка</param>
        /// <param name="delta">Изменение по X и Y</param>
        /// <returns>Точка с новыми координатами</returns>
        public static PointF TranslatePoint(PointF pointToTranslate, PointF delta)
        {
            return new PointF(pointToTranslate.X + delta.X, pointToTranslate.Y + delta.Y);
        }

        /// <summary>
        /// Перемещает точку
        /// </summary>
        /// <param name="pointToTranslate">Заданная точка</param>
        /// <param name="delta">Изменение по X и Y</param>
        public static void TranslatePoint(ref PointF pointToTranslate, PointF delta)
        {
            var newPoint = TranslatePoint(pointToTranslate, delta);
            pointToTranslate.X = newPoint.X;
            pointToTranslate.Y = newPoint.Y;
        }

        /// <summary>
        /// Поворачивает несколько точек
        /// </summary>
        /// <param name="angle">Угол</param>
        /// <param name="points">Точки</param>
        public static void RotatePoints(double angle, ref PointF[] points)
        {
            for (int i = 0; i < points.Length; i++)
            {
                RotatePoint(ref points[i], angle);
            }
        }

        /// <summary>
        /// Поворачивает несколько точек, возвращает новые значения
        /// </summary>
        /// <param name="angle">Угол</param>
        /// <param name="points">Точки</param>
        /// <returns>Точки</returns>
        public static PointF[] RotatePoints(double angle, PointF[] points)
        {
            var newPoints = new PointF[points.Length];
            
            for (int i = 0; i < newPoints.Length; i++)
            {
                newPoints[i] = points[i];
                RotatePoint(ref newPoints[i], angle);
            }

            return newPoints;
        }

        /// <summary>
        /// Перемещает несколько точек
        /// </summary>
        /// <param name="delta">Изменение по X и Y</param>
        /// <param name="points">Точки</param>
        public static void TranslatePoints(PointF delta, ref PointF[] points)
        {
            for (int i = 0; i < points.Length; i++)
            {
                TranslatePoint(ref points[i], delta);
            }
        }

        /// <summary>
        /// Рассчет приращения координат при изменении длины плеча
        /// </summary>
        /// <param name="startPoint">Первая точка манипулятора</param>
        /// <param name="endPoint">Вторая точка манипулятора</param>
        /// <param name="lengthChange">Изменение длины плеча</param>
        /// <returns>Приращение по X и Y</returns>
        public static PointF CalculateDelta(PointF startPoint, PointF endPoint, double lengthChange)
        {
            var dx = endPoint.X - endPoint.X;
            var dy = endPoint.Y - endPoint.Y;
            var angle = Math.Atan2(dy, dx);
            var deltaX = lengthChange * Math.Cos(angle);
            var deltaY = lengthChange * Math.Sin(angle);

            return new PointF((float)deltaX, (float)deltaY);
        }

        /// <summary>
        /// Возвращает середину отрезка, ограниченного двумя точками
        /// </summary>
        /// <param name="firstPoint">Первая точка</param>
        /// <param name="secondPoint">Вторая точка</param>
        /// <returns>Середина</returns>
        public static PointF GetMiddlePoint(PointF firstPoint, PointF secondPoint)
        {
            return new PointF((firstPoint.X + secondPoint.X) / 2, (firstPoint.Y + secondPoint.Y) / 2);
        }

        /// <summary>
        /// Нахождение конца отрезка по углу, длине и начальной точке
        /// </summary>
        /// <param name="beginPoint">Начальная точка</param>
        /// <param name="angle">Угол</param>
        /// <param name="length">Длина</param>
        /// <returns>Конец</returns>
        public static PointF GetEndPoint(PointF beginPoint, double angle, double length)
        {
            var angleInRadians = DegreesToRadians(angle);
            var y = -Math.Sin(angleInRadians)*length + beginPoint.Y;
            var x = Math.Cos(angleInRadians)*length + beginPoint.X;
            return new PointF((float)x, (float)y);
        }

        /// <summary>
        /// Возвращает угол наклона в градусах
        /// </summary>
        /// <param name="firstPoint">Начало отрезка</param>
        /// <param name="secondPoint">Конец отрезка</param>
        /// <returns>Угол</returns>
        public static double GetAngle(PointF firstPoint, PointF secondPoint)
        {
            var dx = secondPoint.X - firstPoint.X;
            var dy = secondPoint.Y - firstPoint.Y;
            return RadiansToDegrees( -Math.Atan2(dy, dx) );
        }

        private static double DegreesToRadians(double value)
        {
            return value * Math.PI / 180;
        }

        private static double RadiansToDegrees(double value)
        {
            return value / Math.PI * 180;
        }
    }
}
