﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Manipulators;

namespace Robot
{
    public class Robot
    {
        public LinkedList<BoundedPart> Parts { get; }

        public Robot()
        {
            Parts = new LinkedList<BoundedPart>();
            var rootPart = new Hinge(-180d, 180d, 0);
            Parts.AddFirst(rootPart);
        }

        public void AddPartToEnd(BoundedPart part)
        {
            if (CanAddPart(part))
                Parts.AddLast(part);
        }

        private bool CanAddPart(BoundedPart part)
        {
            var lastPart = Parts.Last.Value;
            if (lastPart.PartType == PartType.Hinge)
                return part.PartType == PartType.Piston || part.PartType == PartType.Plank;
            return part.PartType == PartType.Hinge;
        }

        public BoundedPart this[int index]
        {
            get { return Parts.ElementAt(index); }
        }
    }
}
