﻿using System;
using System.Globalization;
using Manipulators;
using NUnit.Framework;
using NUnit.Core;

namespace UnitTestProject1
{
    [TestFixture]
    public class ManipulatorsTests
    {
        [TestCase]
        public void HingeCreation()
        {
            Hinge hinge=new Hinge(0.0,90.0,0.0);
            Assert.AreEqual((int)hinge.PartType,0);
        }

        [TestCase()]
        public void HingeCorrectValues()
        {
            Hinge hinge=new Hinge(30,150,48);
            NUnitFramework.Assert.AreEqual(hinge.MaxValue,150);
            Assert.AreEqual(hinge.MaxValue,150);
            Assert.AreEqual(hinge.MinValue,30);
        }

        [TestCase()]
        public void PistonCreation()
        {
            Piston piston=new Piston(10,100,40);
            Assert.AreEqual((int)piston.PartType,1);
        }

        [TestCase()]
        public void PistonCorrectValues()
        {
            Piston piston=new Piston(0,100,50);
            Assert.AreEqual(piston.MaxValue,100);
            Assert.AreEqual(piston.MinValue,0);
            Assert.AreEqual(piston.CurrentValue,50);
        }

        [TestCase()]
        public void PlankCreation()
        {
            Plank plank=new Plank(40);
            Assert.AreEqual(plank.PartType,2);
        }

        [TestCase()]
        public void PlankCorrectValues()
        {
            Plank plank=new Plank(100);
            Assert.AreEqual(plank.CurrentValue,100);
        }
    }
}
