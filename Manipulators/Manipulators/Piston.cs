﻿namespace Manipulators
{
    public class Piston : BoundedPart
    {
        public Piston(double minValue, double maxValue, double value) : base(value, maxValue, minValue)
        {
            PartType = PartType.Piston;
        }
    }
}
