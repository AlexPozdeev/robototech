﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manipulators
{
    public class Plank : BoundedPart
    {
        public Plank(double value) : base(value, value, value)
        {
            PartType = PartType.Plank;
        }
    }
}
