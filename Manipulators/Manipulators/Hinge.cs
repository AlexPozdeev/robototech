﻿namespace Manipulators
{
    public class Hinge : BoundedPart
    {
        public override double MinValue
        {
            get { return _minValue; }
            set
            {
                if (value < -180)
                    value = -180;
                _minValue = value;
                if (CurrentValue < _minValue)
                    CurrentValue = _minValue;
            }
        }

        public override double MaxValue
        {
            get { return _maxValue; }
            set
            {
                if (value > 180)
                    value = 180;
                _maxValue = value;
                if (CurrentValue > _maxValue)
                    CurrentValue = _maxValue;
            }
        }

        public Hinge(double minValue, double maxValue, double value) : base(value, maxValue, minValue)
        {
            PartType = PartType.Hinge;
        }
    }
}
