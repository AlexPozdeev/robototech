﻿using System;
using System.CodeDom;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Manipulators
{
    public enum PartType
    {
        Hinge = 0,
        Piston = 1,
        Plank = 2
    }

    public abstract class BoundedPart
    {
        protected double _minValue;
        protected double _maxValue;
        public double CurrentValue { get; set; }
        public virtual double MinValue
        {
            get { return _minValue; } 
            set { _minValue = value; } 
        }
        public virtual double MaxValue
        {
            get { return _maxValue; }
            set { _maxValue = value; }
        }

        public PointF Position { get; set; }
        public PartType PartType;

        protected BoundedPart(double value, double maxValue, double minValue)
        {
            if (maxValue < minValue)
            {
                MaxValue = minValue;
                MinValue = maxValue;
            }
            else
            {
                MaxValue = maxValue;
                MinValue = minValue;
            }
            ChangeValue(value);
            Position = PointF.Empty;
        }

        public void AddValue(double value)
        {
            var newValue = CurrentValue + value;
            ChangeValue(newValue);
        }

        public void ChangeValue(double value)
        {
            if (value < MinValue)
                CurrentValue = MinValue;
            else if (value > MaxValue)
                CurrentValue = MaxValue;
            else
                CurrentValue = value;
        }
    }
}
