﻿using System;
using System.Collections.Generic;

namespace MatrixProject
{  
    public class CMatrix2D
    {
        //индексация строк-столбцов с нуля
        private int _columnsCount;  //n
        private int _rowsCount;     //m
        private List<double> _elements;

        public CMatrix2D()
        {
            this._columnsCount = 0;
            this._rowsCount = 0;
            this._elements = new List<double>();
        }

        //матрица X * Y
        public CMatrix2D(int columnsCount, int rowsCount, List<double> elements)
        {
            this._columnsCount = columnsCount;
            this._rowsCount = rowsCount;
            this._elements = elements;
        }

        //properties
        #region
        public int GetColumnsCount { get { return this._columnsCount; } }
        public int GetRowsCount { get { return this._rowsCount; } }
        public List<double> GetElements { get { return this._elements; } }
        private bool IsEmpty
        {
            get { return ((this._columnsCount == 0) || (this._rowsCount == 0) || (this._elements.Count == 0)); }
        }

        private bool IsCorrectlyFilled
        {
            get { return ((this._columnsCount * this._rowsCount) == this._elements.Count); }
        }

        public bool IsCorrect
        {
            get { return (!this.IsEmpty && this.IsCorrectlyFilled); }
        }
        #endregion

        public CMatrix2D CreateCopy()
        {
            return (new CMatrix2D(this._columnsCount, this._rowsCount, this._elements));
        }
    
        public double GetItem(int i, int j)
        {
            //i - номер строки, j - столбца
            double res;

            if ((0 <= i) && (i <= _rowsCount - 1) && (0 <= j) && (j <= _columnsCount - 1))
            {
                res = _elements[_columnsCount * i + j];
            }
            else
            {
                throw new ArgumentException("Попытка обращения к несуществующему элементу матрицы: " + 
                    "[" + i.ToString() + "; " + j.ToString() + "]." );
            }
            return res;
        }

        public void SetItem(int i, int j, double value)
        {
            //i - номер строки, j - столбца

            if ((0 <= i) && (i <= _rowsCount - 1) && (0 <= j) && (j <= _columnsCount - 1))
            {
                _elements[_columnsCount * i + j] = value;
            }
            else
            {
                throw new ArgumentException("Попытка обращения к несуществующему элементу матрицы: " +
                    "[" + i.ToString() + "; " + j.ToString() + "].");
            }
        }

        public bool IsEqualTo(CMatrix2D mat)
        {
            bool sizeIsEqual;
            bool elemsAreEqual;

            sizeIsEqual = (this._columnsCount == mat.GetColumnsCount) && (this._rowsCount == mat.GetRowsCount);

            int i = 0;
            int limI = this._elements.Count - 1;

            elemsAreEqual = false;
            while ((i <= limI) && (elemsAreEqual = (this._elements[i] == mat._elements[i])))
                i++;

            return (sizeIsEqual && elemsAreEqual);
        }

        public List<double> GetRow(int index)
        {
            List<double> row = null;

            if ((0 <= index) && (index <= this._rowsCount - 1))
            {
                row = new List<double>();
                
                int j;
                int limJ = this._columnsCount - 1;
                for (j = 0; j <= limJ; j++)
                    row.Add(this.GetItem(index, j));
            }

            return row;
        }

        public List<double> GetColumn(int index)
        {
            List<double> col = null;

            if ((0 <= index) && (index <= this._columnsCount - 1))
            {
                col = new List<double>();

                int i;
                int limI = this._rowsCount - 1;
                for (i = 0; i <= limI; i++)
                    col.Add(this.GetItem(i, index));
            }

            return col;
        }

        //скалярное произведение строки на столбец
        public double RowColumnMult(List<double> row, List<double> col)
        {
            if (row.Count != col.Count) throw new ArgumentException(@"Попытка скалярного произведения строки и столбца, 
                состоящих из разного количества элементов.");
            else
            {
                double res = 0; 

                int i;
                int limI = row.Count - 1; //или col.Count
                for (i = 0; i <= limI; i++)
                    res += row[i] * col[i];
                
                return res;
            }
        }

        //умножение матриц
        public CMatrix2D Mult(CMatrix2D mat)
        {
            List<double> elems = new List<double>();
            //
            int i, j;
            //заполнение построчно
            int limI = this._rowsCount - 1;
            int limJ = mat._columnsCount - 1;
            for (i = 0; i <= limI; i++) //or mat._rowsCount
                for (j = 0; j <= limJ; j++)
                {
                    double elem = this.RowColumnMult(this.GetRow(i), mat.GetColumn(j));
                    elems.Add(elem);
                }
            return new CMatrix2D(mat.GetColumnsCount, this._rowsCount, elems);
        }

        //транспонирование
        public void Transpose()
        {
            int i, j;
            int limI, limJ;

            limI = this._columnsCount - 1;
            limJ = this._rowsCount - 1;
            for (i = 0; i <= limI; i++)
                for (j = 0; j <= limJ; j++)
                {
                    if (j > i)
                    {
                        double tmp;

                        tmp = this.GetItem(i, j);
                        this.SetItem(i, j, this.GetItem(j, i));
                        this.SetItem(j, i, tmp);
                    }
                }
        }
    }
}
