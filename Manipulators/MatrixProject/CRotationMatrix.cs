﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixProject
{
    //функциональный класс нахождения матрицы поворота
    public static class CRotationMatrix
    {
        //придумайте названия сами, пожалуйста

        //правосторонняя система координат и положительное направление вращения против часовой стрелки
        //или
        //левосторонняя система координат и положительное направление вращения по часовой стрелке
        public static CMatrix2D FirstVariant(double degreeAngle)
        {
            double radAngle = degreeAngle / 180.0 * Math.PI;
            CMatrix2D mat = new CMatrix2D(2, 2, new List<double> { Math.Cos(radAngle), 
                -1 * Math.Sin(radAngle), Math.Sin(radAngle), Math.Cos(radAngle) });

            return mat;
        }

        //правосторонняя система координат и отрицательное направление вращения по часовой стрелке
        //или
        //левосторонняя система координат и отрицательное направление вращения против часовой стрелке
        public static CMatrix2D SecondVariant(double degreeAngle)
        {
            double radAngle = degreeAngle / 180.0 * Math.PI;
            CMatrix2D mat = new CMatrix2D(2, 2, new List<double> { Math.Cos(radAngle), 
                Math.Sin(radAngle), -1 * Math.Sin(radAngle), Math.Cos(radAngle) });

            return mat;
        }
    }
}
