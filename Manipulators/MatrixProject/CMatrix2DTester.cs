﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MatrixProject;

namespace ConsoleClientServer
{
    [TestFixture]
    class CMatrix2DTester
    {
        [TestCase]
        public void TestMatrixIsCorrect()
        {
            //negatives
            CMatrix2D m1 = new CMatrix2D();
            Assert.IsFalse(m1.IsCorrect);
            //no columns
            CMatrix2D m2 = new CMatrix2D(0, 2, new List<double> {2, 4, 6, 8 }); 
            Assert.IsFalse(m2.IsCorrect);
            //no rows
            CMatrix2D m3 = new CMatrix2D(2, 0, new List<double> { 2, 4, 6, 8 });
            Assert.IsFalse(m3.IsCorrect);
            //too many elems
            CMatrix2D m4 = new CMatrix2D(2, 2, new List<double> { 2, 4, 6, 8, 10 });
            Assert.IsFalse(m4.IsCorrect);
            //too few elems
            CMatrix2D m5 = new CMatrix2D(2, 2, new List<double> { 2, 4, 6 }); 
            Assert.IsFalse(m5.IsCorrect);
            //no elems
            CMatrix2D m6 = new CMatrix2D(2, 2, new List<double> {/*empty*/}); 
            Assert.IsFalse(m6.IsCorrect);

            //positives
            //1 col - some rows
            CMatrix2D m7 = new CMatrix2D(1, 3, new List<double> {1, 3, 5 });
            Assert.IsTrue(m7.IsCorrect);
            //some cols - 1 row
            CMatrix2D m8 = new CMatrix2D(3, 1, new List<double> {1, 3, 5 });
            Assert.IsTrue(m8.IsCorrect);
            //some cols  - some rows
            CMatrix2D m9 = new CMatrix2D(3, 3, new List<double> {1, 3, 5, 7, 9, 11, 13, 15, 17 });
            Assert.IsTrue(m9.IsCorrect);
        }

        [TestCase]
        public void TestMatrixIsEqualTo()
        {
            //same address
            CMatrix2D m8 = new CMatrix2D(3, 1, new List<double> { 1, 3, 5 });
            Assert.IsTrue(m8.IsEqualTo(m8));
            //different ones
            CMatrix2D m10 = new CMatrix2D(3, 1, new List<double> { 1, 3, 5 });
            Assert.IsTrue(m10.IsEqualTo(m8));
        }

        [TestCase]
        public void TestMatrixCreateCopy()
        {
            CMatrix2D m8 = new CMatrix2D(3, 1, new List<double> { 1, 3, 5 });
            CMatrix2D mCopy = m8.CreateCopy();

            Assert.IsTrue(m8.IsEqualTo(mCopy));
        }

        [TestCase]
        public void TestMatrixGetItem()
        {
            //квадратная матрица 3х3 с гл. диагональю 1-2-3
            //индексы --------------------------------------- 00 01 02 10 11 12 20 21 22
            CMatrix2D m9 = new CMatrix2D(3, 3, new List<double> { 1, 0, 0, 0, 2, 0, 0, 0, 3 });
            Assert.AreEqual(1, m9.GetItem(0, 0));
            Assert.AreEqual(2, m9.GetItem(1, 1));
            Assert.AreEqual(3, m9.GetItem(2, 2));

            //прямоуг. матрица 3х1 (строка)
            //индексы --------------------------------------- 00 01 02
            CMatrix2D m7 = new CMatrix2D(3, 1, new List<double> { 1, 3, 5 });
            Assert.AreEqual(1, m7.GetItem(0, 0));
            Assert.AreEqual(3, m7.GetItem(0, 1));
            Assert.AreEqual(5, m7.GetItem(0, 2));

            //прямоуг. матрица 1х3 (столбец)
            //индексы --------------------------------------- 00 10 20
            CMatrix2D m8 = new CMatrix2D(1, 3, new List<double> { 2, 4, 6 });
            Assert.AreEqual(2, m8.GetItem(0, 0));
            Assert.AreEqual(4, m8.GetItem(1, 0));
            Assert.AreEqual(6, m8.GetItem(2, 0));
        }

        [TestCase]
        public void TestMatrixSetItem()
        {
            //квадратная матрица 3х3 из нулей
            //индексы ---------------------------------------   00 01 02 10 11 12 20 21 22
            CMatrix2D m = new CMatrix2D(3, 3, new List<double> { 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            for (int i = 0; i <= m.GetColumnsCount - 1; i++)
                for (int j = 0; j <= m.GetRowsCount - 1; j++)
                    if (i == j)
                        m.SetItem(i, j, 1);
            Assert.AreEqual(1, m.GetItem(0, 0));
            Assert.AreEqual(1, m.GetItem(1, 1));
            Assert.AreEqual(1, m.GetItem(2, 2));
        }

        [TestCase]
        public void TestMatrixGetRow()
        {
            //квадратная матрица 3х3 с гл. диагональю 1-2-3
            //индексы --------------------------------------- 00 01 02 10 11 12 20 21 22
            CMatrix2D m9 = new CMatrix2D(3, 3, new List<double> { 1, 0, 0, 0, 2, 0, 0, 0, 3 });
            List<double> row = new List<double>();
            row = m9.GetRow(1);
            Assert.AreEqual(3, row.Count);
            Assert.AreEqual(0, row[0]);
            Assert.AreEqual(2, row[1]);
            Assert.AreEqual(0, row[2]);
        }

        [TestCase]
        public void TestMatrixGetColumn()
        {
            //квадратная матрица 3х3 с гл. диагональю 1-2-3
            //индексы --------------------------------------- 00 01 02 10 11 12 20 21 22
            CMatrix2D m9 = new CMatrix2D(3, 3, new List<double> { 1, 0, 0, 0, 2, 0, 0, 0, 3 });
            List<double> col = new List<double>();
            col = m9.GetColumn(2);
            Assert.AreEqual(3, col.Count);
            Assert.AreEqual(0, col[0]);
            Assert.AreEqual(0, col[1]);
            Assert.AreEqual(3, col[2]);
        }

        [TestCase]
        public void TestRowColumnMult()
        {
            CMatrix2D m = new CMatrix2D();
            Assert.AreEqual(47, m.RowColumnMult(new List<double> { 1, 4 }, new List<double> { 7, 10 }));
            Assert.AreEqual(71, m.RowColumnMult(new List<double> { 2, 5 }, new List<double> { 8, 11 }));
            Assert.AreEqual(99, m.RowColumnMult(new List<double> { 3, 6 }, new List<double> { 9, 12 }));
        }

        [TestCase]
        public void TestMatrixMult()
        {
            //прямоуг. матрицы
            CMatrix2D a = new CMatrix2D(2, 3, new List<double> { 1, 4, 2, 5, 3, 6 });
            CMatrix2D b = new CMatrix2D(3, 2, new List<double> { 7, 8, 9, 10, 11, 12 });
            CMatrix2D c = new CMatrix2D();
            c = a.Mult(b);
            Assert.IsTrue(c.IsEqualTo(new CMatrix2D(3, 3, new List<double> { 47, 52, 57, 64, 71, 78, 81, 90, 99 })));

            //квадратные матрицы
            a = new CMatrix2D(4, 4, new List<double> { 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8 });
            //умножим саму на себя
            c = new CMatrix2D();
            c = a.Mult(a);
            Assert.IsTrue(c.IsEqualTo(new CMatrix2D(4, 4,
                new List<double> { 34, 44, 54, 64, 82, 108, 134, 160, 34, 44, 54, 64, 82, 108, 134, 160 })));

            a = new CMatrix2D(2, 2, new List<double> { 1, 2, 3, 4 });
            b = new CMatrix2D(2, 2, new List<double> { 5, 6, 7, 8 });
            c = new CMatrix2D();
            c = a.Mult(b);
            Assert.IsTrue(c.IsEqualTo(new CMatrix2D(2, 2,
               new List<double> { 19, 22, 43, 50 })));
        }

        [TestCase]
        public void TestMatrixTranspose()
        {
            //квадратная матрица 3х3
            //индексы ---------------------------------------   00 01 02 10 11 12 20 21 22
            CMatrix2D m = new CMatrix2D(3, 3, new List<double> { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            CMatrix2D mExpected = new CMatrix2D(3, 3, new List<double> { 1, 4, 7, 2, 5, 8, 3, 6, 9 });
            m.Transpose();

            Assert.IsTrue(m.IsEqualTo(mExpected));
        }

    }
}
